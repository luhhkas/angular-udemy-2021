import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-username',
  templateUrl: './username.component.html',
  styleUrls: ['./username.component.css']
})
export class UsernameComponent implements OnInit {
  userName: string = '';
  allowNewUser: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  checkUsername() {
    if (this.userName === '') return true;
    else return false;
  }

  onResetUserName() {
    this.userName = '';
  }
}
