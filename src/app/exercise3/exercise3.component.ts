import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exercise3',
  templateUrl: './exercise3.component.html',
  styleUrls: ['./exercise3.component.css']
})
export class Exercise3Component implements OnInit {
  displayPwd = false;
  numberOfClicks = [];

  constructor() { }

  ngOnInit(): void {
  }

  onDisplayPwd() {
    this.displayPwd = this.displayPwd === false ? true : false;
    this.numberOfClicks.push(`Clicked ${this.numberOfClicks.length+1} in the button`);
  }

}
